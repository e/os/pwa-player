package foundation.e.pwaplayer.database

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.hamcrest.Matchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class PwaTest {

    private lateinit var database: PwaDatabase

    @Before
    fun createDatabase() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PwaDatabase::class.java
        ).build()
    }

    @After
    fun closeDatabase() {
        database.close()
    }

    @Test
    fun insertAndCount() {
        assertThat(database.pwaDao().count(), `is`(0))
        val pwa = Pwa(shortcutId = "test-shortcut", url = "https://e.foundation")
        database.pwaDao().insert(pwa)
        assertThat(database.pwaDao().count(), `is`(1))
    }
}