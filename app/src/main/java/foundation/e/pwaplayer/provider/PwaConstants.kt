package foundation.e.pwaplayer.provider

import android.net.Uri
import android.provider.BaseColumns._ID

class PwaConstants {
    companion object {
        const val ID = _ID
        const val TITLE = "title"
        const val SHORTCUT_ID = "shortcutId"
        const val URL = "url"
        const val ICON = "icon"

        const val TABLE_NAME = "pwa"
        val CONTENT_URI = Uri.parse("content://$AUTHORITY/$TABLE_NAME")
    }
}