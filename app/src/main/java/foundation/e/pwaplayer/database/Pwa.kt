package foundation.e.pwaplayer.database

import android.content.ContentValues
import android.database.Cursor
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import foundation.e.pwaplayer.provider.PwaConstants
import java.lang.IllegalArgumentException

@Entity(
    tableName = PwaConstants.TABLE_NAME,
    indices = [Index(value = [PwaConstants.SHORTCUT_ID], unique = true)]
)
data class Pwa(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = PwaConstants.ID)
    var id: Long = 0,

    @ColumnInfo(name = PwaConstants.SHORTCUT_ID)
    var shortcutId: String,

    @ColumnInfo(name = PwaConstants.URL)
    var url: String,

    @ColumnInfo(name = PwaConstants.TITLE)
    val title: String,

    @ColumnInfo(name = PwaConstants.ICON, typeAffinity = ColumnInfo.BLOB)
    val iconBlob: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Pwa

        if (id != other.id) return false
        if (shortcutId != other.shortcutId) return false
        if (url != other.url) return false
        if (title != other.title) return false
        if (!iconBlob.contentEquals(other.iconBlob)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + shortcutId.hashCode()
        result = 31 * result + url.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + iconBlob.contentHashCode()
        return result
    }
}

fun Cursor.mapToPwa(): Pwa {
    val url = this.getString(this.getColumnIndex(PwaConstants.URL))
    val shortcutId = this.getString(this.getColumnIndex(PwaConstants.SHORTCUT_ID))
    val id = this.getLong(this.getColumnIndex(PwaConstants.ID))
    val title = this.getString(this.getColumnIndex(PwaConstants.TITLE))
    val icon = this.getBlob(this.getColumnIndex(PwaConstants.ICON))
    return Pwa(id, shortcutId, url, title, icon)
}

fun ContentValues.mapToPwa(): Pwa {
    val url = if (this.containsKey("URL")) this.getAsString("URL")
    else throw IllegalArgumentException(
        "Pwa URL can't be null"
    )
    val shortcutId = if (this.containsKey("SHORTCUT_ID")) this.getAsString("SHORTCUT_ID")
    else throw IllegalArgumentException(
        "Pwa Shortcut Id can't be null"
    )

    val title = if (this.containsKey("TITLE")) this.getAsString("TITLE")
    else throw IllegalArgumentException(
        "Pwa Title can't be null"
    )
    val icon = if (this.containsKey("ICON")) this.getAsByteArray("ICON")
    else throw IllegalArgumentException(
        "Pwa Icon can't be null"
    )
    return Pwa(shortcutId = shortcutId, url = url, title = title, iconBlob = icon)
}