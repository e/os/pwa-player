/*
 * Copyright (C) 2022  ECORP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.pwaplayer.broadcast

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent

/**
 * This class is responsible for sending PWA install / removal status to all apps.
 * The sent intent contains following extras:
 * 1. SHORTCUT_ID - string shortcut id.
 * 2. URL - string url of the pwa. This is to be set as Intent data to launch the PWA.
 * 3. PWA_ID - long type id of the PWA in PWA Player database. This id is needed to launch a PWA.
 *
 * An app should declare a broadcast receiver in the manifest as below:
 * ```
 * <receiver android:name=".receiver.PWAPlayerStatusReceiver"
 *     android:exported="true">
 *     <intent-filter>
 *         <action android:name="foundation.e.pwaplayer.PWA_ADDED" />
 *     </intent-filter>
 *     <intent-filter>
 *         <action android:name="foundation.e.pwaplayer.PWA_CHANGED" />
 *     </intent-filter>
 *     <intent-filter>
 *         <action android:name="foundation.e.pwaplayer.PWA_REMOVED" />
 *     </intent-filter>
 *     </receiver>
 * ```
 * Example code of receiving a PWA broadcast and launching a PWA can be found here:
 * https://gitlab.e.foundation/SayantanRC/pwa-broadcast-and-launch-test
 *
 * This class will be able to manually resolve the eligible broadcast receivers in all apps
 * and send them the broadcast.
 */
object PWAStatusBroadcast {

    private fun broadcast(
        application: Application,
        action: String,
        shortcutId: String,
        url: String,
        pwaId: Long
    ) {
        try {
            val pm = application.packageManager
            val intent = Intent(action).apply {
                putExtra("SHORTCUT_ID", shortcutId)
                putExtra("URL", url)
                putExtra("PWA_ID", pwaId)
            }
            pm.queryBroadcastReceivers(intent, 0).forEach {
                val component = ComponentName(it.activityInfo.packageName, it.activityInfo.name)
                application.sendBroadcast(intent.setComponent(component))
            }
        }
        catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun broadcastPwaAdded(context: Context, shortcutId: String, url: String, pwaId: Long) {
        broadcast(context.applicationContext as Application,
            "foundation.e.pwaplayer.PWA_ADDED", shortcutId, url, pwaId)
    }

    fun broadcastPwaChanged(context: Context, shortcutId: String, url: String, pwaId: Long) {
        broadcast(context.applicationContext as Application,
            "foundation.e.pwaplayer.PWA_CHANGED", shortcutId, url, pwaId)
    }

    fun broadcastPwaRemoved(context: Context, shortcutId: String, url: String, pwaId: Long) {
        broadcast(context.applicationContext as Application,
            "foundation.e.pwaplayer.PWA_REMOVED", shortcutId, url, pwaId)
    }
}