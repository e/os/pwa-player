package foundation.e.pwaplayer.ui.about

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import foundation.e.pwaplayer.BuildConfig
import foundation.e.pwaplayer.R

class AboutFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.about_preference, rootKey)

        val buildVersionPreference =
            findPreference<Preference>(getString(R.string.key_build_version))
        buildVersionPreference?.summary = BuildConfig.VERSION_NAME
    }

}