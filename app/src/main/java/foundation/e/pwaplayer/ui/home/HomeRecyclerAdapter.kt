package foundation.e.pwaplayer.ui.home

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import foundation.e.pwaplayer.R
import foundation.e.pwaplayer.database.Pwa
import foundation.e.pwaplayer.ui.player.PwaActivity
import foundation.e.pwaplayer.util.toBitmap

class HomeRecyclerAdapter(
    private val context: Context,
    private var items: List<Pwa> = emptyList()
) :
    RecyclerView.Adapter<HomeRecyclerAdapter.PwaViewHolder>() {

    inner class PwaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTextView: TextView = itemView.findViewById(R.id.titlePwa)
        val iconView: ImageView = itemView.findViewById(R.id.iconPwa)

        init {
            itemView.setOnClickListener {
                val intent = Intent(context, PwaActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_RETAIN_IN_RECENTS)
                intent.action = "foundation.e.blisslauncher.VIEW_PWA"
                intent.data = Uri.parse(items[adapterPosition].url)
                intent.putExtra(PwaActivity.KEY_PWA_ID, items[adapterPosition].id)
                context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PwaViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_pwa, parent, false)
        return PwaViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PwaViewHolder, position: Int) {
        val pwa = items[position]
        holder.titleTextView.text = pwa.title
        items[position].iconBlob.toBitmap()?.also {
            holder.iconView.setImageBitmap(it)
        }
    }

    fun updateData(items: List<Pwa>) {
        this.items = items
        notifyDataSetChanged()
    }
}
