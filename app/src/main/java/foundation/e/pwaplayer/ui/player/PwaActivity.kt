package foundation.e.pwaplayer.ui.player

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager.TaskDescription
import android.content.ContentUris
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.view.KeyEvent
import android.view.View
import android.webkit.WebView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import foundation.e.apps.pwa.PwaWebChromeClient
import foundation.e.pwaplayer.R
import foundation.e.pwaplayer.database.mapToPwa
import foundation.e.pwaplayer.provider.PwaConstants
import foundation.e.pwaplayer.util.toBitmap

class PwaActivity : AppCompatActivity() {

    private var pwaWebChromeClient: PwaWebChromeClient? = null
    private var webView: WebView? = null

    private val workerThread: HandlerThread = HandlerThread("WORKER-THREAD")
    private lateinit var workerHandler: Handler
    private val uiHandler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        if (intent == null) {
            Toast.makeText(this, "URL can't be empty", Toast.LENGTH_SHORT).show()
            finish()
        }

        val id = getIntent().getLongExtra(KEY_PWA_ID, -1)

        workerThread.start()
        workerHandler = Handler(workerThread.looper)
        workerHandler.post {
            setPwaTaskInfo( id)
        }
        setContentView(R.layout.activity_pwa)
    }

    private fun setPwaTaskInfo(id: Long) {
        val cursor = contentResolver.query(
            ContentUris.withAppendedId(PwaConstants.CONTENT_URI, id),
            null, null, null, null
        )
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val pwa = cursor.mapToPwa()
                val icon = pwa.iconBlob?.toBitmap()
                val name = pwa.title
                uiHandler.post { updateTaskInfo(name, icon) }
            }
            cursor.close()
        }
    }

    private fun updateTaskInfo(name: String, icon: Bitmap?) {
        this.title = name
        val taskDescription: TaskDescription
        if (icon != null) {
            taskDescription = TaskDescription(name, icon)
            setTaskDescription(taskDescription)
        } else {
            taskDescription = TaskDescription(name)
        }
        setTaskDescription(taskDescription)

        pwaWebChromeClient = PwaWebChromeClient(this, name)
        webView = findViewById<View>(R.id.webView) as WebView
        setWebView(webView, pwaWebChromeClient!!)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setWebView(webView: WebView?, pwaWebChromeClient: PwaWebChromeClient) {
        val webSettings = webView!!.settings
        webSettings.javaScriptEnabled = true
        webSettings.allowContentAccess = true
        webSettings.loadWithOverviewMode = true
        webSettings.allowFileAccess = true
        webSettings.allowUniversalAccessFromFileURLs = true
        webSettings.allowContentAccess = true
        webSettings.allowFileAccessFromFileURLs = true
        webSettings.domStorageEnabled = true
        val start_url = intent.data.toString()
        var scope = intent.getStringExtra(KEY_SCOPE)
        if (scope == null) scope = ""
        webView.webChromeClient = pwaWebChromeClient
        webView.webViewClient = PwaWebViewClient(start_url, scope)
        webView.loadUrl(start_url)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PwaWebChromeClient.REQUEST_LOCATION_PERMISSION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pwaWebChromeClient!!.locationPermissionGranted()
                } else {
                    pwaWebChromeClient!!.locationPermissionDenied()
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView != null && webView!!.canGoBack()) {
            webView!!.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (requestCode == PwaWebChromeClient.REQUEST_FILE_CHOOSER) {
            pwaWebChromeClient!!.onActivityResult(resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        const val KEY_PWA_NAME = "PWA_NAME"
        const val KEY_PWA_ICON = "PWA_ICON"
        const val KEY_PWA_ID = "PWA_ID"
        const val KEY_SCOPE = "pwa_scope"
        private const val TAG = "PwaActivity"
    }
}