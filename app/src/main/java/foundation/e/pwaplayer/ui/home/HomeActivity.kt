package foundation.e.pwaplayer.ui.home

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import foundation.e.pwaplayer.R
import foundation.e.pwaplayer.database.Pwa
import foundation.e.pwaplayer.database.mapToPwa
import foundation.e.pwaplayer.provider.PwaConstants.Companion.CONTENT_URI
import foundation.e.pwaplayer.ui.about.AboutActivity
import kotlinx.android.synthetic.main.activity_home.*

private const val TAG = "HomeActivity"

class HomeActivity : AppCompatActivity() {

    private val adapter: HomeRecyclerAdapter by lazy { HomeRecyclerAdapter(this) }

    private val pwaObserver: PwaObserver by lazy { PwaObserver(this) }

    private val workerThread: HandlerThread = HandlerThread("WORKER-THREAD")
    private lateinit var workerHandler: Handler
    private val uiHandler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(toolbar)
        title = resources.getString(R.string.installed_pwas)

        val linearLayoutManager = LinearLayoutManager(this)
        pwaRecyclerView.layoutManager = linearLayoutManager
        pwaRecyclerView.setHasFixedSize(true)
        pwaRecyclerView.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        pwaRecyclerView.adapter = adapter

        workerThread.start()
        workerHandler = Handler(workerThread.looper)
        workerHandler.post {
            loadData()
        }
    }

    private fun loadData() {
        contentResolver.registerContentObserver(CONTENT_URI, true, pwaObserver)
        refreshData()
    }

    override fun onDestroy() {
        super.onDestroy()
        contentResolver.unregisterContentObserver(pwaObserver)
        pwaObserver.clear()
        workerThread.quit()
    }

    fun refreshData() {
        val cursor = contentResolver.query(CONTENT_URI, null, null, null, null)
        val data = ArrayList<Pwa>()
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val pwa = cursor.mapToPwa()
                    data.add(pwa)
                } while (cursor.moveToNext())
            }
            cursor.close()
        }
        uiHandler.post {
            adapter.updateData(data)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.home_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings -> {
                startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                    data = Uri.fromParts("package", packageName, null)
                })
                true
            }
            R.id.about -> {
                startActivity(Intent(this, AboutActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}